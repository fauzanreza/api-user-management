'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error('Bad Request',validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamPostOneUser = async (payload) => {
    let constraints = {};
    let values = {};
    constraints[payload.name] = {length: {minimum: 5}};
    constraints[payload.password] = {length: {minimum: 5}};
    values[payload.name] = payload.name;
    values[payload.password] = payload.password;
    return await validateConstraints(values,constraints);
}

module.exports = {
    isValidParamPostOneUser: isValidParamPostOneUser,
}