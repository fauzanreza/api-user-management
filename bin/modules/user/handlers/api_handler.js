'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator')

const getUser = async (req, res, next) => {
  const getData = async () => {
    return await queryHandler.getAllUser();
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await getData());
}

const postDataLogin = async (req, res, next) => {
  const payload = req.body;
  const postRequest = async () => {
    return await commandHandler.postDataLogin(payload);
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await postRequest());
}

const postOneUser = async (req, res, next) => {
  const payload = req.body;
  const validateParam = await validator.isValidParamPostOneUser(payload);
  const postRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.postDataRegister(payload);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await postRequest(validateParam));
}

module.exports = {
  postOneUser,
  postDataLogin,
  getUser
}