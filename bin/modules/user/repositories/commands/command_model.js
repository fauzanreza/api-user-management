'use strict';

const generalUser = () => {
    const model = {
        username:``,
        password:``,
        created_at:``,
        updated_at:``   
    }
    return model;
}

module.exports = {
    generalUser: generalUser
}
