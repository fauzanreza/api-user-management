'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const User = require('./domain');

const getAllUser = async (userId) => {
    const getData = async () => {
        const user = new User();
        const result = await user.viewAllUser();
        return result;
    }
    const result = await getData();
    return result;
}

module.exports = {
    getAllUser
}