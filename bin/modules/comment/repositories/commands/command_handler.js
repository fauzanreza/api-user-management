'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Comment = require('./domain');

const postOneComment = async (payload) => {
    const comment = new Comment();
    const postCommand = async (payload) => {
        return await comment.addNewComment(payload);
    }
    return postCommand(payload);
}

const deleteOneComment = async (id) => {
    const comment = new Comment();
    const delCommand = async (id) => {
        return await comment.deleteComment(id);
    }
    return delCommand(id);
}


module.exports = {
    postOneComment : postOneComment,
    deleteOneComment : deleteOneComment
}