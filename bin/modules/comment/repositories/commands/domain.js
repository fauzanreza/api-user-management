'use strict';

const nconf = require('nconf');
const rp = require('request-promise');
const model = require('./command_model');
const command = require('./command');
const query = require('../queries/query');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');
const validate = require('validate.js');
const logger = require("../../../../helpers/utils/logger");
const SNS = require('../../../../helpers/components/aws-sns/sns');
const Emitter = require('../../../../helpers/events/event_emitter');
const EventPublisher = require('../../../../helpers/events/event_publisher');

class Comment{

    async addNewComment(payload){
        const data = [payload];
        let view = model.generalComment();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.comment)){accumulator.comment = value.comment;}     
            return accumulator;
        }, view);
        const document = view;
        const result = await command.insertOneComment(document);
        return result;
    }

    async updateComment(params, payload){
        const data = [payload];
        let view = model.generalComment();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
            if(!validate.isEmpty(value.comment)){accumulator.comment = value.comment;}       
            return accumulator;
        }, view);
        const document = view;
        const result = await command.updateOneComment(params, document);
        return result;
    }

    async deleteComment(params){
        const result = await command.deleteOneComment(params);
        return result;
    }

}

module.exports = Comment;