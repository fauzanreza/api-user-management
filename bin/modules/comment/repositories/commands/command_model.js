'use strict';

const generalComment = () => {
    const model = {
        id:``,
        comment: ``,
        createdAt:``,
        updatedAt:``,    
    }
    return model;
}

module.exports = {
    generalComment: generalComment
}
