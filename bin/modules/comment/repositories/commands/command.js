'use strict';

const MySQL = require('../../../../helpers/databases/mariadb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneComment = async (document) => {
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const table =  'comment';
    const recordset = await db.postOne(table, document);
    return recordset;
}

const updateOneComment = async (params, document) => {
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const table =  'comment';
    const id = params.id
    const recordset = await db.updateOne(table, document, id);
    return recordset;
}

const deleteOneComment = async (params) => {
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const table =  'comment';
    const id = params.id
    const recordset = await db.deleteOne(table, id);
    return recordset;
}

module.exports = {
    insertOneComment: insertOneComment,
    updateOneComment: updateOneComment,
    deleteOneComment: deleteOneComment
}