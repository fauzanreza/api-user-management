'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/validator');
const Comment = require('./domain');

const getOneComment = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const comment = new Comment(queryParam);
        const result = await comment.viewOneComment();
        return result;
    }
    const result = await getQuery(queryParam);
    return result;
}


const getAllComments = async (queryParam) => {
    const getQuery = async (queryParam) => {
        const comment = new Comment(queryParam);
        const result = await comment.viewAllComments();
        return result;
    }

    const result = await getQuery(queryParam);
    return result;
}


module.exports = {
    getOneComment : getOneComment,
    getAllComments : getAllComments
}