'use strict';

const MySQL = require('../../../../helpers/databases/mariadb/db');
const config = require('../../../../infra/configs/global_config');
const ObjectId = require('mongodb').ObjectId;

const findOneComment = async (params) => {
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const table =  'comment';
    const id = params.id
    const recordset = await db.findData(table, id);
    return recordset;
}
const findAllComments = async () => {
    const db = new MySQL(config.getDevelopmentDBMySQL());
    const table =  'comment';
    const recordset = await db.findMany(table);
    return recordset;
}


module.exports = {
    findOneComment: findOneComment,
    findAllComments: findAllComments
}