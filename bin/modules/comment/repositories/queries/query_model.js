'use strict';

const generalItem = () => {
    const model = {
        id:``,
        comment: ``,
        createdAt:``,
        updatedAt:``,      
    }
    return model;
}

module.exports = {
    generalItem: generalItem
}
