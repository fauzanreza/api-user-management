'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const validator = require('../utils/validator');
// const queryParser = require('../utils/query_parser');
const queryHandler = require('../repositories/queries/query_handler');
const commandHandler = require('../repositories/commands/command_handler');

const getOneComment = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParamGetOneComment(queryParam);
  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getOneComment(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}


const getAllComments = async (req, res, next) => {
  const queryParam = req.params;
  const validateParam = await validator.isValidParamGetAllComments(queryParam);

  const getRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await queryHandler.getAllComments(queryParam);
    }
  }

  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res, 'success', result, `Your Request Has Been Processed`);
  }

  sendResponse(await getRequest(validateParam));
}


const postOneComment = async (req, res, next) => {
  const payload = req.body;
  const validateParam = await validator.isValidParamPostOneComment(payload);
  const postRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.postOneComment(payload);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await postRequest(validateParam));
}

const deleteOneComment = async (req, res, next) => {
  const payload = req.params;
  const validateParam = await validator.isValidParamGetOneComment(payload);
  const deleteRequest = async (result) => {
    if(result.err){
      return result;
    }else{
      return await commandHandler.deleteOneComment(payload);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'fail',result) : 
    wrapper.response(res,'success',result,`Your Request Has Been Processed`);
  }
  sendResponse(await deleteRequest(validateParam));
}

module.exports = {
  getOneComment: getOneComment,
  getAllComments: getAllComments,
  postOneComment: postOneComment,
  deleteOneComment: deleteOneComment
}